#!/usr/bin/env node
'use strict';
const
	BCRYPT_ROUNDS = 12,
	bcrypt = require('bcrypt'),
	bluebird = require('bluebird'),
	pr = require('prompt'),
	redis = require('redis')
	;

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
const dbh = redis.createClient();

pr.start();
pr.get({
	'properties': {
		'username': {
			'required': true
		},
		'password': {
			'hidden': true,
			'required': true
		},
		'name': {
			'description': 'Full name',
			'required': true
		},
		'props': {
			'description': 'Additional properties (JSON object)',
			'default': '{}',
			'required': true,
			'before': (val) => { return JSON.parse(val); }
		}
	}
}, (err, res) => {
	if (err) {
		throw err;
	}
	bcrypt.hash(res.password, BCRYPT_ROUNDS, (err, hash) => {
		if (err) {
			throw err;
		}
		res.props.username = res.username;
		res.props.name = res.name;
		res.props.hash = hash;
		const args = [];
		Object.keys(res.props).forEach((k) => {
			args.push(k);
			args.push(res.props[k]);
		});
		dbh.multi()
			.hmset(`hex:user:${res.username}@local`, args)
			.persist(`hex:user:${res.username}@local`)
			.execAsync()
			.then(process.exit, console.error);
	});
});
