'use strict';
const fs = require('fs');

module.exports = {
	'http': {
		'port': 8001,
		'proxy': true,
		'proxyBase': '/login',
		'headers': {
			'access-control-allow-origin': '*'
		}
	},
	'log': {
		'auth': `${__dirname}/log/auth`
	},
	'passport': {
		'base': '/',
		'strategies': {
			'local': {
				'blockUserIpPairs': {
					'attempts': 5,
					'forSeconds': 60 * 60
				}
			},
			'google': {
				'callback': 'http://bronson.hubzero.org/login/google/callback'
			},
			'shibboleth': {}
		}
	}
};
