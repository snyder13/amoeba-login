'use strict';
module.exports = {
	'share-session': {
		'description': 'Export login sessions to registered parties',
		'deps': [ 'google', 'local', 'shibboleth' ].map((strategy) => { return `hex-authn-passport.${strategy}`; })
	}
};
