'use strict';
const
	URL = require('url').URL,
	uuid = require('uuid/v4'),
	jwt = require('jsonwebtoken'),
	dur = require('dur');

module.exports = ({ app, conf, log }) => {
	const fwdDomains = conf.get('passport.forwardDomains', {});
	app.loginHandlers.unshift(function (req, _res, next) {
		if (!req.session || !req.session.loginReturn) {
			log.debug('share-session: no session or no return', { 'session': req.session });
			return next();
		}
		const ret = Buffer.from(req.session.loginReturn, 'base64').toString();

		// local
		if (/^\//.test(ret)) {
			log.debug('share-session: local return');
			req.session.loginReturn = ret;
			return next();
		}
		if (Object.keys(fwdDomains).length === 0) {
			log.debug('share-session: empty fwdDomains');
			delete req.session.loginReturn;
			return next();
		}
		let parsed;
		try {
			parsed = new URL(ret);
		}
		catch (ex) {
			log.debug('share-session: failed to parse URL', { 'url': ret });
			delete req.session.loginReturn;
			return next();
		}
		if (!Object.keys(fwdDomains).some((domain) => {
			if (domain === parsed.hostname) {
				log.debug('share-session: found forward domain');
				const expiry = conf.get('passport.local.jwt.expiry', '60s');
				const token = jwt.sign(Object.assign(JSON.parse(req.session.passport.user), { 'expiry': dur(expiry) / 1000 }), fwdDomains[domain], { 'algorithm': 'RS256', 'expiresIn': expiry, 'jwtid': uuid() });
				req.session.loginReturn = "" + ret + (ret.indexOf('?') > -1 ? '&' : '?') + "jwt=" + token;
				return true;
			}
			return false;
		})) {
			log.debug('share-session: no forward domain matched', { parsed });
			delete req.session.loginReturn;
		}
		return next();
	});
};
