<?php

if (!isset($_GET['jwt'])) {
	header('Location: http://bronson.hubzero.org/login?return='.base64_encode($_SERVER['SCRIPT_URI']));
	exit();
}

require 'vendor/autoload.php';
use \Firebase\JWT\JWT;

try {
	print_r(JWT::decode($_GET['jwt'], file_get_contents(__DIR__.'/../bronson.pub'), array('RS256')));
	// @FIXME store jti for at least the expiry length to prevent double-spending
}
catch (\Exception $ex) {
	echo $ex;
}

echo '<hr>';
highlight_file(__FILE__);
