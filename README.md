## What
This is an example of how an authentication service might work, using a few pilot providers: local, Google+, and Shibboleth.

It implements these strategies with Passport.js (and with the Shibboleth native SP software for that case).

A list of domains that can log in with the service are maintained along with keys that are used to sign JSON web tokens describing a given user's session. So, an application on a registered domain can redirect users to https://thislogin.service/login?return=(base64-encoded-url-on-their-domain). The user logs in, and they're sent back to that URL with the query parameter `jwt` added.

## Dependencies
 1. [Redis](https://redis.io/download)
 2. [node.js & npm](https://nodejs.org/en/download/)
 3. [Apache](https://httpd.apache.org/download.cgi)

## Installation
Run `npm install` in the package root

## Configuration
It's possible to run this stand-alone, but the assumption in the shipped configuration is that it is reverse-proxied by Apache.

        ProxyPass "/login" "http://localhost:8001"
        ProxyPassReverse "/login" "http://localhost:8001"

You can run `node_modules/hex/bin/add-user.js` to add a local user. `secrets.js.dist` has some information about how to get Google authentication set up. Shibboleth requires a bit too much setup to fit here, but message me if you really want to know.

The server writes an authentication log that should be assessed by `fail2ban`, but I haven't written any of the rules for it yet.

## Running
Run `DEBUG=hex:* npm start` in the package root. (`npm install -g pm2` once and then `pm2 start ./ecosystem.json` subsequently to run monitored)

## Testing
There is a minimal PHP client in the `client/` path. Put those files somewhere where they will be interpreted by Apache or FPM, and run `composer install` to get the library used for JWT parsing.

There is also a `bin/add-user.js` that can be used to put an account in Redis for testing. Just run it and enter data as prompted.

There are a few things to edit in the PHP file, namely the redirect URL and the location of the public part of the key pair.

After that, if all is well, visiting this page should redirect to the login service, which should return with a JSON web token that gets verified and printed.
